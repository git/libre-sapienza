/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef NETWORK_H
#define NETWORK_H


#include "config.h"

#include <curl/curl.h>


/* base address to make calls to the server */
#define BASE_URL "https://www.studenti.uniroma1.it/phoenixws"

#define URL_BUFFER_SIZE 512

extern char url_buffer []; /* buffer used to prepare the url for each call */

/* authentication token by the server */
extern char *token; 


/* libcurl object used for all talking to the server */
extern CURL *curl_handle; 


extern char *response;    /* buffer for the body of http responses */
extern size_t resp_s;     /* size of the response buffer */
extern size_t resp_c;     /* cursor of the next element to write in the buffer; response [resp_c] should always be \0 */


/* callback called by the curl library with the body of the http response */
size_t write_callback (char *ptr, size_t size, size_t nmemb, void *userdata);


/* empty the response buffer, without freeing any memory */
void empty_response (void);


/* make a login attempt with global student id and given password and update global authentication token.
   return a status code. 1 for success and meaningful token, 0 for failure. the function prints some error messages on stderr */
int request_token (const char *pass);


/* the following functions requests info to the server and return 1 on success and 0 on failure */

/* request exams you can register for and store the json response in *available_exams */
int request_available_exams (void);


int request_completed_exams (void);


int request_active_reservations (void);


/* request available dates for an exam, given the index number (starting from zero) into available_exams */
int request_available_exam_dates (int index);


/* try to reserve exam, taking the index in the session array */
int reserve_exam (int sess_n);


int delete_reservation (int n);


int request_student_info (void);


int request_student_photo (void);


/* perform a HTTP GET request to the server at the given url. Most infostud functions are accessed this way */
int perform_get_request (const char *url);



#endif  /* NETWORK_H */
