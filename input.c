/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>

#include "gettext.h"
#define _(String) gettext (String)

#include <readline/readline.h>

#include "input.h"


/* read string with line editing. the line must be freed */
const char *
readline_string (const char *prompt)
{
  char *str = NULL;
  str = readline (prompt);
  return str;
}


/* read password with disabled echo and using readline, showing a given prompt on the same line  */
const char *
readline_password (const char *prompt)
{
  char *pass = NULL;
  struct termios t_old, t_new;

  /* disable echo */
  tcgetattr (fileno (stdin), &t_old);
  t_new = t_old;
  t_new.c_lflag &= ~ECHO;
  tcsetattr (fileno (stdin), TCSAFLUSH, &t_new);

  /* read password with line editing. readline discards the newline */
  pass = readline (prompt);

  /* put a newline, because echo is off */
  putchar ('\n');

  /* restore echo */
  tcsetattr (fileno (stdin), TCSAFLUSH, &t_old);

  return pass;
}


/* read an unsigned integer using readline, showing a given prompt on the same line */
unsigned int
readline_uint (const char *prompt)
{
  char *buff;
  int ret;

  buff = readline (prompt);

  /* convert to integer */
  ret = (unsigned int) strtol (buff, NULL, 10);
  
  free (buff);
  return ret;
}


/* print prompt and read user action */
int
prompt_and_read_user_action (void)
{
  printf (_("\n***\n"
	    "1 - print available exams\n"
	    "2 - print completed exams\n"
	    "3 - print active reservations\n"
	    "4 - print student info\n"
	    "5 - print paid taxes\n"
	    "6 - print unpaid taxes\n"
	    "7 - exit\n\n"));
  return readline_uint ("> ");
}
