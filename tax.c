/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "config.h"

#include <stdio.h>
#include <jansson.h>

#include "gettext.h"
#define _(String) gettext (String)

#include "network.h"
#include "main.h"
#include "tax.h"


/* Keep these global variables as cache */

/* paid taxes in this year */
json_t *paid_taxes = NULL;

/* taxes yet to pay */
json_t *unpaid_taxes = NULL;


/* ask the server about taxes already paid */
int
request_paid_taxes (void)
{  
  json_error_t error;
  
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/contabilita/%d/%s?ingresso=%s", studentid, "bollettinipagati", token); 
  perform_get_request (url_buffer);
  
  paid_taxes = json_loads (response, 0, &error);
  if (!paid_taxes)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  return 1;
}


/* ask the server about taxes yet to pay */
int
request_unpaid_taxes (void)
{  
  json_error_t error;
  
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/contabilita/%d/%s?ingresso=%s", studentid, "bollettininonpagati", token); 
  perform_get_request (url_buffer);
  
  unpaid_taxes = json_loads (response, 0, &error);
  if (!unpaid_taxes)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  return 1;
}


void
print_taxes_json (int paid)
{
  json_t *ris;
  
  if (paid)
    ris = json_object_get (paid_taxes, "risultatoLista");
  if (!paid)
    ris = json_object_get (unpaid_taxes, "risultatoLista");
  
  json_t *taxes = json_object_get (ris, "risultati");
  
  json_t *x;
  int i;
  
  for (i = 0, x = json_array_get (taxes, 0); x; x = json_array_get (taxes, ++i))
    {
      printf (_("%2d - code %s\n"
		"      course %s\n"
		"      %s\n"
		"      year %lld\n"), i+1,
	      json_string_value (json_object_get (x, "codiceBollettino")),
	      json_string_value (json_object_get (x, "corsoDiStudi")),
	      json_string_value (json_object_get (x, "descCorsoDiStudi")),
	      json_integer_value (json_object_get (x, "annoAcca")));
      if (paid)
	printf (_("      %s € paid\n"
		  "      paid on %s\n\n"),
		json_string_value (json_object_get (x, "impoVers")),
		json_string_value (json_object_get (x, "dataVers")));
      if (!paid)
	printf (_("      %s € due\n"
		  "      deadline %s\n\n"),
		json_string_value (json_object_get (x, "importoBollettino")),
		json_string_value (json_object_get (x, "scadenza")));
    }
  if (i==0 && paid)
    printf ("there is no paid tax\n");
  if (i==0 && !paid)
    printf ("there is no tax to pay\n");
}

