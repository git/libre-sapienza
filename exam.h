/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef EXAM_H
#define EXAM_H


#include "config.h"

#include <jansson.h>


/* list of available exams that you can register for */
extern json_t *available_exams;

/* list of completed exams */
extern json_t *completed_exams;

/* list of active reservations */
extern json_t *active_reservations;

/* list of available dates for a single given exam */
extern json_t *available_exam_dates;


/* chained list of exams */
struct exam_l
{
  const char *codice_insegnamento;
  const char *codice_modulo_didattico;
  const char *codice_corso_insegnamento;
  int  cfu;
  const char *descrizione;
  const char *ssd;

  struct exam_l *next;
};


/* free an exam_l list */
void free_exam_list (struct exam_l *ex_l);


/* create a list of available exams from the json array sent by the server */
struct exam_l *load_available_exams (const json_t *ex);


/* print an exam list in a nice way */
void print_exam_list (const struct exam_l *ex_l);


/* print an exam list as sent by the server in a nice way */
void print_exams_json (const json_t *ex);


void print_available_exams (void);


void print_available_exams_briefly (void);


void print_completed_exams (void);


void print_active_reservations (void);


/* print active reservations, one per line */
int print_active_reservations_briefly (void);


void print_available_exam_dates (void);


/* print information about a single exam date that can be reserved */
void print_given_exam_date (int exam_n, int sess_n);

  
#endif /* EXAM_H */
  
