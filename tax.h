/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef TAX_H
#define TAX_H


#include "config.h"

#include <jansson.h>


/* paid taxes in this year */
extern json_t *paid_taxes;


/* taxes yet to pay */
extern json_t *unpaid_taxes;


/* ask the server about taxes already paid */
int request_paid_taxes (void);


void print_taxes_json (int);


int request_unpaid_taxes (void);


#endif  /* TAX_H */
