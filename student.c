/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "config.h"

#include <curl/curl.h>
#include <jansson.h>

#include "gettext.h"
#define _(String) gettext (String)

#include "student.h"


/* Student information as sent by the server. We keep it as cache */

json_t *student_info = NULL;


#define STINFO_GET_STR(var, field) (json_string_value (json_object_get (var, field)))
#define STINFO_GET_INT(var, field) (json_integer_value (json_object_get (var, field)))


void
print_student_info (void)
{
  json_t *rit = json_object_get (student_info, "ritorno");
  char name [100];

  /* properly handle the alias career */
  if (strcmp ("N", STINFO_GET_STR (rit, "isAlias")))
    strncpy (name, STINFO_GET_STR (rit, "aliasNome"), 100);
  else
    strncpy (name, STINFO_GET_STR (rit, "nome"), 100);
  
  printf (_("%s %s\n\n"
	    
	    "fiscal code %s\n"
	    "date of birth %s\n"
	    "gender %s\n"
	    "alias career %s\n"
	    "citizenship %s\n"
	    "mail address %s\n"
	    "institutional mail address %s\n\n"

	    "student id %lld\n"
	    "faculty %s\n"
	    "course %s (code %s)\n"
	    "total credits %s\n"
	    "course year %s\n"
	    "%s\n"
	    "first enrollment %s\n"
	    "last enrollment %s\n\n"

	    "erasmus student: %s\n\n"),
	  STINFO_GET_STR (rit, "cognome"),
	  name,
	  STINFO_GET_STR (rit, "codiceFiscale"),
	  STINFO_GET_STR (rit, "dataDiNascita"),
	  STINFO_GET_STR (rit, "sesso"),
	  STINFO_GET_STR (rit, "isAlias"),
	  STINFO_GET_STR (rit, "cittadinanza"),
	  STINFO_GET_STR (rit, "indiMail"),
	  STINFO_GET_STR (rit, "indiMailIstituzionale"),
	  STINFO_GET_INT (rit, "matricola"),
	  STINFO_GET_STR (rit, "facolta"),
	  STINFO_GET_STR (rit, "nomeCorso"),
	  STINFO_GET_STR (rit, "codCorso"),
	  STINFO_GET_STR (rit, "creditiTotali"),
	  STINFO_GET_STR (rit, "annoCorso"),
	  STINFO_GET_STR (rit, "tipoIscrizione"),
	  STINFO_GET_STR (rit, "primaIscr"),
	  STINFO_GET_STR (rit, "ultIscr"),
	  json_is_true (json_object_get (rit, "isErasmus")) ? "yes" : "no");
}
