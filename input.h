/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef INPUT_H
#define INPUT_H


#include "config.h"


/* read password with disabled echo and using readline, showing a given prompt on the same line  */
const char *readline_password (const char *prompt);


/* read an unsigned integer using readline, showing a given prompt on the same line */
unsigned int readline_uint (const char *prompt);


/* print prompt and read user action */
int prompt_and_read_user_action (void);


#endif /* INPUT_H */
