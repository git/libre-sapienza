/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "config.h"

#include <jansson.h>

#include "gettext.h"
#define _(String) gettext (String)

#include "exam.h"
#include "util.h"
#include "network.h"


/* We keep all the following variables global, as a cache.
   The request* functions download information from the server.
   The caller of those functions should check that these variables 
   are not null. If they aren't, and it still wants to query the 
   server, for example because the user wants an update, then it should 
   properly free them */


/* list of available exams that you can register for */
json_t *available_exams = NULL;

/* list of completed exams */
json_t *completed_exams = NULL;

/* list of active reservations */
json_t *active_reservations = NULL;

/* list of available dates for a single given exam */
json_t *available_exam_dates = NULL;


/* create a list of available exams from the json array sent by the server */
struct exam_l *
load_available_exams (const json_t *ex)
{
  json_t *x;
  int i;
  struct exam_l *ret = NULL, *next = NULL;
  x = json_array_get (ex, 0);
  if (x)
    {
      ret = xmalloc (sizeof(struct exam_l));

      ret->codice_insegnamento = json_string_value (json_object_get (x, "codiceInsegnamento"));
      ret->codice_modulo_didattico = json_string_value (json_object_get (x, "codiceModuloDidattico"));
      ret->codice_corso_insegnamento = json_string_value (json_object_get (x, "codiceCorsoInsegnamento"));
      ret->cfu = json_integer_value (json_object_get (x, "cfu"));
      ret->descrizione = json_string_value (json_object_get (x, "descrizione"));
      ret->ssd = json_string_value (json_object_get (x, "ssd"));
    }
  
  next = ret;
  
  for (i = 1, json_decref (x), x = json_array_get (ex, 1); x; i++, json_decref (x), x = json_array_get (ex, i))
    {
      next->next = xmalloc (sizeof(struct exam_l));
      
      next = next->next;
      
      next->codice_insegnamento = json_string_value (json_object_get (x, "codiceInsegnamento"));
      next->codice_modulo_didattico = json_string_value (json_object_get (x, "codiceModuloDidattico"));
      next->codice_corso_insegnamento = json_string_value (json_object_get (x, "codiceCorsoInsegnamento"));
      next->cfu = json_integer_value (json_object_get (x, "cfu"));
      next->descrizione = json_string_value (json_object_get (x, "descrizione"));
      next->ssd = json_string_value (json_object_get (x, "ssd"));
      
    }

  next->next = NULL;

  return ret;
}


/* free an exam list */
void
free_exam_list (struct exam_l *ex_l)
{
  struct exam_l *tmp;

  while (ex_l)
    {
      tmp = ex_l;
      ex_l = ex_l->next;
      tmp->next = NULL;
      free (tmp);
    }
}


/* print an exam list in a nice way */
void
print_exam_list (const struct exam_l *ex_l)
{
  int i = 1;
  
  for (; ex_l; i++, ex_l = ex_l->next)
    {
      printf (_("%2d - %s\n"
		"      %d cfu\n"
		"      %s\n\n"),
	      i, ex_l->descrizione, ex_l->cfu, ex_l->ssd);
    }
}


/* print an exam list as sent by the server in a nice way */
void
print_exams_json (const json_t *ex)
{
  json_t *esami = json_object_get (json_object_get (ex, "ritorno"), "esami");

  json_t *x;
  int i;
  
  for (i = 0, x = json_array_get (esami, 0); x; x = json_array_get (esami, ++i))
    {
      printf (_("%2d - %s\n"
		"      %.1f cfu\n"
		"      %s\n\n"),
	      i+1, json_string_value (json_object_get (x, "descrizione")),
	      json_real_value (json_object_get (x, "cfu")),
	      json_string_value (json_object_get (x, "ssd")));

    }
  if (i==0)
    printf ("there is no exam\n");

  putchar ('\n');
}


void print_available_exams (void)
{
  print_exams_json (available_exams);
  if (json_is_true (json_object_get (available_exams, "pdSApprovato")))
    printf (_("exam plan is approved\n\n"));
  else
    printf (_("there's no approved exam plan\n\n"));

}


void print_available_exams_briefly (void)
{
  json_t *esami = json_object_get (json_object_get (available_exams, "ritorno"), "esami");

  json_t *x;
  int i;
  
  for (i = 0, x = json_array_get (esami, 0); x; x = json_array_get (esami, ++i))
    {
      printf (_("%2d - %s, %.1f cfus, %s\n"),
	      i+1, json_string_value (json_object_get (x, "descrizione")),
	      json_real_value (json_object_get (x, "cfu")),
	      json_string_value (json_object_get (x, "ssd")));
    }
  if (i==0)
    printf (_("there is no exam\n"));

  putchar ('\n');
}


void
print_completed_exams (void)
{
  print_exams_json (completed_exams);
}


void
print_active_reservations (void)
{
  print_exams_json (active_reservations);
}


/* print active reservations, one per line */
int
print_active_reservations_briefly (void)
{
  json_t *appelli = json_object_get (json_object_get (active_reservations, "ritorno"), "appelli");

  json_t *x;
  int i;
  
  for (i = 0, x = json_array_get (appelli, 0); x; x = json_array_get (appelli, ++i))
    {
      printf (_("%2d - %s, %.1f cfus, %s, %s\n"),
	      i+1, json_string_value (json_object_get (x, "descrizione")),
	      json_real_value (json_object_get (x, "cfu")),
	      json_string_value (json_object_get (x, "ssd")),
	      json_string_value (json_object_get (x, "dataAppe")));
    }
  if (i==0)
    printf (_("there is no reservation\n"));

  putchar ('\n');

  return i;
}

void
print_available_exam_dates (void)
{
  json_t *rit = json_object_get (available_exam_dates, "ritorno");
  json_t *appelli = json_object_get (rit, "appelli");

  json_t *x;
  int i;

  i = 0;
  x = json_array_get (appelli, 0);

  if (x)
    printf (_("%s, %.1f cfus\n\n"), json_string_value (json_object_get (x, "descrizione")),
	    json_real_value (json_object_get (x, "crediti")));
		      
  for (; x; x = json_array_get (appelli, ++i))
    {
      printf (_("%2d - %lld\n"
		"      channel %s\n"
		"      teacher %s\n"
		"      date %s\n"
		"      reservable between %s and %s\n"
		"      notes: %s\n\n"),
	      i+1, json_integer_value (json_object_get (x, "codAppe")),
	      json_string_value (json_object_get (x, "canale")),
	      json_string_value (json_object_get (x, "docente")),
	      json_string_value (json_object_get (x, "dataAppe")),
	      json_string_value (json_object_get (x, "dataInizioPrenotazione")),
	      json_string_value (json_object_get (x, "dataFinePrenotazione")),
	      json_string_value (json_object_get (x, "note")));
    }
  if (i==0)
    printf (_("there is no session available\n\n"));
}


/* print information about a single exam date that can be reserved */
void
print_given_exam_date (int exam_n, int sess_n)
{
  if (!available_exams)
    request_available_exams ();
}
