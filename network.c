/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/* Code for speaking with the servers */


#include "config.h"

#include <stdio.h>
#include <string.h>

#include <curl/curl.h>
#include <jansson.h>

#include "gettext.h"
#define _(String) gettext (String)

#include "network.h"
#include "util.h"
#include "main.h"
#include "exam.h"
#include "student.h"


char url_buffer [URL_BUFFER_SIZE]; /* buffer used to prepare the url for each call */

char *token; /* authentication token by the server */

CURL *curl_handle; /* libcurl object used for all talking to the server */

char *response = NULL; /* buffer for the body of http responses */
size_t resp_s = 0;     /* size of the response buffer */
size_t resp_c = 0;     /* cursor of the next element to write in the buffer; response [resp_c] should always be \0 */
 


/* callback called by the curl library with the body of the http response.
   as specified by the library doc, it returns the number of processed bytes */
size_t
write_callback (char *ptr, size_t size, size_t nmemb, void *userdata)
{
  size_t realsize = size * nmemb;
  if (resp_c + realsize > resp_s)
    {
      response = realloc (response, resp_c + realsize);
    }
  if (!response)
    { 
      fprintf (stderr, _("memory exhausted!\n"));
      return 0;
    }
  resp_s = resp_c + realsize;
  memcpy (response + resp_c, ptr, realsize);
  resp_c += realsize;
  response [resp_c] = 0;
 
  return realsize;
}


/* empty the response buffer, without freeing any memory */
void
empty_response (void)
{
  if (response)
    resp_c = response [0] = 0;
}


/* make a login attempt with global student id and given password and update global authentication token.
   return a status code. 1 for success and meaningful token, 0 for failure. the function prints some error messages on stderr */
int
request_token (const char *pass)
{
  struct curl_slist *headers;
  CURLcode ret;
  json_t *root, *esito, *flag_esito_j, *output;
  json_int_t flag_esito;
  json_error_t error;
  int retval = 0;
  
  curl_easy_setopt (curl_handle, CURLOPT_URL, BASE_URL "/autenticazione");

  /* create a linked list of header fields; this is freed by the end of the function */
  headers = curl_slist_append (NULL, "Accept: application/json");
  headers = curl_slist_append (headers, "Content-EventType: application/x-www-form-urlencoded");
  curl_easy_setopt (curl_handle, CURLOPT_HTTPHEADER, headers);

  snprintf (url_buffer, URL_BUFFER_SIZE, "key=1nf0r1cc1&matricola=%d&stringaAutenticazione=%s", studentid, pass);
  curl_easy_setopt (curl_handle, CURLOPT_POSTFIELDS, url_buffer);

  /* prepare the response buffer */
  /*response = xmalloc (2048);
  resp_s = 2048;
  resp_c = 0;
  response [0] = 0;*/

  empty_response ();

  curl_easy_setopt (curl_handle, CURLOPT_WRITEFUNCTION, write_callback);
  ret = curl_easy_perform (curl_handle);
  if (ret != CURLE_OK)
    {
      fprintf (stderr, _("curl_easy_perform() failed: %s\n"),
	       curl_easy_strerror (ret));
      return 0;
    }

#ifdef DEBUG_PRINT_JSON
  printf ("%s\n", response);
#endif
  
  root = json_loads (response, 0, &error);
  if (!root)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  
  esito = json_object_get (root, "esito");
  if (!esito)
    {
      fprintf (stderr, _("server sent a response i can't understand. maybe the protocol changed?\n"));
      return 0;
    }
  
  flag_esito_j = json_object_get (esito, "flagEsito");
  if (!flag_esito_j)
    {
      fprintf (stderr, _("server sent a response i can't understand. maybe the protocol changed?\n"));
      return 0;
    }
    
  flag_esito = json_integer_value (flag_esito_j);
    
  switch (flag_esito)
    {
    case -6:
      fprintf (stderr, _("invalid student id or password\n"));
      retval = 0;
      goto clean_and_return;
    case -4:
      fprintf (stderr, _("user not enabled for Infostud service\n"));
      retval = 0;
      goto clean_and_return;
    case -2:
      fprintf (stderr, _("password expired\n"));
      retval = 0;
      break;
    case -1:
      fprintf (stderr, _("invalid credentials\n"));
      retval = 0;
      goto clean_and_return;
    case 0:
      printf (_("login successful!\n"));
      retval = 1;
      break;
    default:
      fprintf (stderr, _("i don't know what the hell is going on\n"));
      retval = 0;
      goto clean_and_return;
    }

  output = json_object_get (root, "output");
  if (!output)
    {
      fprintf (stderr, _("server sent a response i can't understand. maybe the protocol changed?\n"));
      return 0;
    }
  
  if (token)
    {
      free (token);
      token = NULL;
    }
  token = xmalloc (64);
  strncpy (token, json_string_value (output), 64);

  /* decrement reference counts */
  json_decref (root);

 clean_and_return:

  /* reset header and fields, plus free linked list of headers */
  curl_easy_setopt (curl_handle, CURLOPT_HTTPHEADER, NULL);
  curl_easy_setopt (curl_handle, CURLOPT_POSTFIELDS, NULL);
  curl_slist_free_all (headers);

  return retval;
}


/* request available exams to the server */
int
request_available_exams (void)
{
  json_error_t error;

  if (available_exams)
    {
      json_decref (available_exams);
      available_exams = NULL;
    }
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/studente/%d/insegnamentisostenibili?ingresso=%s", studentid, token);
  perform_get_request (url_buffer);
  
  available_exams = json_loads (response, 0, &error);
  if (!available_exams)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  return 1;
}


int
request_completed_exams (void)
{
  json_error_t error;

  if (completed_exams)
    {
      json_decref (completed_exams);
      completed_exams = NULL;
    }
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/studente/%d/esamiall?ingresso=%s", studentid, token);    
  perform_get_request (url_buffer);
  
  completed_exams = json_loads (response, 0, &error);
  if (!completed_exams)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  return 1;
}


int
request_active_reservations (void)
{
  json_error_t error;

  if (active_reservations)
    {
      json_decref (active_reservations);
      active_reservations = NULL;
    }
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/studente/%d/prenotazioni?ingresso=%s", studentid, token);
  perform_get_request (url_buffer);
  
  active_reservations = json_loads (response, 0, &error);
  if (!active_reservations)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  return 1;
}


/* request available dates for an exam, given the index number (starting from zero) into available_exams */
int
request_available_exam_dates (int index)
{
  json_error_t error;
  const char *modulecode;
  const char *coursecode;
  const char *academicyear;
  json_t *rit, *esami, *ex;
  
  if (!student_info)
    {
      request_student_info ();
    }
  if (!available_exams)
    {
      request_available_exams ();
    }
  if (available_exam_dates)
    {
      json_decref (available_exam_dates);
      available_exam_dates = NULL;
    }
  
  rit = json_object_get (available_exams, "ritorno");
  esami = json_object_get (rit, "esami");
  ex = json_array_get (esami, index);
  
  modulecode = json_string_value (json_object_get (ex, "codiceModuloDidattico"));
  coursecode = json_string_value (json_object_get (ex, "codiceCorsoInsegnamento"));
  
  rit = json_object_get (student_info, "ritorno");
  academicyear = json_string_value (json_object_get (rit, "annoAccaAtt"));
  
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/appello/ricerca?ingresso=%s&tipoRicerca=%s&criterio=%s&codiceCorso=%s&annoAccaAuto=%s",
	    token, "4", modulecode, coursecode, academicyear);
  printf ("%s\n", url_buffer);
  perform_get_request (url_buffer);
  
  available_exam_dates = json_loads (response, 0, &error);
  if (!available_exam_dates)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  return 1;
}


/* try to reserve exam, taking the index in the session array */
int
reserve_exam (int sess_n)
{
  int cod_verbale;
  int cod_appello;
  const char *cod_corso;
  json_t *rit, *appelli, *appello;

  rit = json_object_get (available_exam_dates, "ritorno");
  appelli = json_object_get (rit, "appelli");
  appello = json_array_get (appelli, sess_n);
  
  cod_verbale = json_integer_value (json_object_get (appello, "codIdenVerb"));
  cod_appello = json_integer_value (json_object_get (appello, "codAppe"));
  cod_corso = json_string_value (json_object_get (appello, "codCorsoStud"));
  
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/prenotazione/%d/%d/%s/0/?ingresso=%s",
	    cod_verbale, cod_appello, cod_corso, token);

  perform_get_request (url_buffer);

  return 0;
}


int
delete_reservation (int n)
{
  return 0;
}

int
request_student_info (void)
{
  json_error_t error;

  if (student_info)
    {
      json_decref (student_info);
      student_info = NULL;
    }
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/studente/%d?ingresso=%s", studentid, token);
  perform_get_request (url_buffer);
  student_info = json_loads (response, 0, &error);
  if (!student_info)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  return 1;
}


int
request_student_photo (void)
{
  json_error_t error;
  
  snprintf (url_buffer, URL_BUFFER_SIZE, BASE_URL "/studente/%d/esamiall?ingresso=%s", studentid, token);    
  perform_get_request (url_buffer);
  
  completed_exams = json_loads (response, 0, &error);
  if (!completed_exams)
    {
      fprintf (stderr, _("error: on line %d: %s\n"), error.line, error.text);
      return 0;
    }
  return 1;
}


/* perform a HTTP GET request to the server at the given url. Most infostud functions are accessed this way */
int
perform_get_request (const char *url)
{
  CURLcode ret;

  empty_response ();
  curl_easy_setopt (curl_handle, CURLOPT_URL, url);
  curl_easy_setopt (curl_handle, CURLOPT_WRITEFUNCTION, write_callback);
  curl_easy_setopt (curl_handle, CURLOPT_HTTPGET, 1L);
  curl_easy_setopt (curl_handle, CURLOPT_HTTPHEADER, NULL);
  
  ret = curl_easy_perform (curl_handle);
  if (ret != CURLE_OK)
    {
      fprintf (stderr, _("curl_easy_perform() failed: %s\n"),
	       curl_easy_strerror (ret));
      return 0;
    }

#ifdef DEBUG_PRINT_JSON
  fprintf (stderr, "%s\n", response);
#endif
  
  return 1;
}


