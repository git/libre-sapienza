/*  Copyright (C) 2020 Andrea G. Monaco
 *
 *  This file is part of libre-sapienza.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "config.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

#include <curl/curl.h>
#include <jansson.h>

#include "gettext.h"
#define _(String) gettext (String)
  
#include <readline/readline.h>

#include "exam.h"
#include "input.h"
#include "main.h"
#include "network.h"
#include "tax.h"
#include "student.h"


#define STREQ(a,b) (!strcmp(a,b))


unsigned int studentid; /* student id of the current login */


int
main (int argc, char *argv[])
{
  const char *pass = NULL;  /* password */
  unsigned int exam_n;               /* exam chosen by user */
  unsigned int sess_n;               /* session chosen by user during reservation */
  char prompt [64];
  
  /* user choice in the main menu */
  char *choice = NULL;

  /* initialize gettext for translation */
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);
  
  if (curl_global_init (CURL_GLOBAL_ALL))
    {
      fprintf (stderr, _("could not initialize libcurl, which is the library used for network operations.\n"
			 "please file a bug report\n"));
      exit (1);
    }

  curl_handle = curl_easy_init ();
  if (!curl_handle)
    {
      fprintf (stderr, _("could not create libcurl handle\n"));
      exit (1);
    }

  print_startup_message ();
  
  studentid = readline_uint (_("student id: "));
  pass = readline_password (_("password: "));
  request_token (pass);

  if (!token)
    {
      fprintf (stderr, _("could not get authentication token. exiting...\n"));
      exit (1);
    }

  putchar ('\n');
  while (1)
    {
      if (choice)
	free (choice);
      choice = readline ("> ");

      if (STREQ (choice, "h"))
	{
	  print_help ();
	}

      else if (STREQ (choice, "l"))
	{
	  studentid = readline_uint (_("student id: "));
	  pass = readline_password (_("password: "));
	  request_token (pass);
	}

      else if (STREQ (choice, "p"))
	{
	  if (!available_exams)
	    request_available_exams ();
	  
	  print_available_exams_briefly ();

	  int exam_total = json_array_size (json_object_get (json_object_get (available_exams, "ritorno"), "esami"));
	  snprintf (prompt, 64, _("exam (1-%d, or 0 to cancel): "), exam_total); 
	  exam_n = readline_uint (prompt);

	  if (!exam_n || exam_n > exam_total)
	    {
	      if (exam_n > exam_total)
		printf (_("that doesn't exist!\n\n"));
	      
	      continue;
	    }
	      
	  /* index provided by user starts by one, internal one starts from zero */
	  request_available_exam_dates (exam_n - 1); 
	  print_available_exam_dates ();
	  int sess_total = json_array_size (json_object_get (json_object_get (available_exam_dates, "ritorno"), "appelli"));
	  if (!sess_total)
	    continue;
	  
	  snprintf (prompt, 64, _("session (1-%d, or 0 to cancel): "), sess_total);
	  sess_n = readline_uint (prompt);

	  if (!sess_n || sess_n > sess_total)
	    {
	      if (sess_n > sess_total)
		printf (_("that doesn't exist!\n\n"));
	      continue;
	    }
	  
	  reserve_exam (sess_n-1);
	  request_active_reservations ();
	}

      else if (STREQ (choice, "d"))
	{
	  if (!active_reservations)
	    request_active_reservations ();

	  int tot_res = print_active_reservations_briefly ();
	  if (!tot_res)
	    continue;
	  snprintf (prompt, 64, _("reservation (1-%d, or 0 to cancel): "), tot_res); 
	  int res_n = readline_uint (prompt);
	  if (!res_n || res_n > tot_res)
	    {
	      if (res_n > tot_res)
		printf (_("that doesn't exist!\n"));
	      
	      continue;
	    }
	  delete_reservation (res_n - 1);
	}
	      
      else if (STREQ (choice, "a") || STREQ (choice, "a!"))
	{
	  if (!available_exams || STREQ (choice, "a!"))
	    request_available_exams ();
	  print_available_exams ();
	}

      else if (STREQ (choice, "c") || STREQ (choice, "c!"))
	{
	  if (!completed_exams || STREQ (choice, "c!"))
	    request_completed_exams ();
	  print_completed_exams ();
	}

      else if (STREQ (choice, "r") || STREQ (choice, "r!"))
	{
	  if (!active_reservations || STREQ (choice, "r!"))
	    request_active_reservations ();
	  print_active_reservations ();
	}

      else if (STREQ (choice, "s") || STREQ (choice, "s!"))
	{
	  if (!student_info || STREQ (choice, "s!"))
	    request_student_info ();
	  print_student_info ();
	}

      else if (STREQ (choice, "t") || STREQ (choice, "t!"))
	{
	  if (!paid_taxes || STREQ (choice, "t!"))
	    request_paid_taxes ();
	  print_taxes_json (1);
	}

      else if (STREQ (choice, "u") || STREQ (choice, "u!"))
	{
	  if (!unpaid_taxes || STREQ (choice, "u!"))
	    request_unpaid_taxes ();
	  print_taxes_json (0);
	}

      else if (STREQ (choice, "q"))
	break;
      
      else
	fprintf (stderr, _("not an option!\n\n"));
    }

  printf (_("goodbye! exiting...\n"));
  curl_easy_cleanup (curl_handle);
  
  return 0;
}


void
print_version_and_exit (void)
{
  printf (_("libre-sapienza 0.1\n"
	    "Copyright (C) 2020 Andrea G. Monaco.\n"
	    "License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>\n"
	    "This is free software: you are free to change and redistribute it.\n"
	    "There is NO WARRANTY, to the extent permitted by law.\n"));

  exit (0);
}


void
print_help (void)
{
  printf (_("Commands:\n\n"
	    ""
	    "q - quit\n"
	    "h - print this help\n\n"
	    ""
	    "p - reserve exam\n"
	    "d - delete reservation\n\n"
	    ""
	    "the following commands, when followed by !, request information from the server\n"
	    "even if cache is available. use this feature wisely\n\n"
	    ""
	    "l - repeat login\n"
	    "a - print available exams\n"
	    "c - print completed exams\n"
	    "r - print active reservations\n"
	    "s - print student's info\n"
	    "t - print paid taxes\n"
	    "u - print unpaid taxes\n\n"
	    ""
	    "Report bugs to: andrea.monaco@autistici.org\n"
	    "libre-sapienza home page: https://www.nongnu.org/libre-sapienza\n\n"));
}


void
print_startup_message (void)
{
  printf (_("libre-sapienza, copyright (C) 2020  Andrea G. Monaco\n"
	    "This program comes with ABSOLUTELY NO WARRANTY.\n"
	    "This is free software released under the GNU GPL 3 or later.\n"
	    "See the COPYING file for the full license.\n\n"
	    ""
	    "type h for help\n\n"));
}
